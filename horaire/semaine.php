<?php
require_once('session.php');
class semaine extends session{
    function _contruct($numero, $dateDebut, $dateFin){
        $this -> numero = $numero;
        $this -> dateDebut= $dateDebut;
        $this -> dateFin= $dateFin;
    }

    function setNumero($numero){
        $this->numero=$numero;
    }

    function getNumero() {
        return $this->numero;
    }

    function setDateDebut($dateDebut){
        $this->dateDebut=$dateDebut;
    }

    function getDateDebut() {
        return $this->dateDebut;
    }

    function setDateFin($dateFin){
        $this->dateFin=$dateFin;
    }

    function getDateFin() {
        return $this->dateFin;
    }
}
$semaine1 = new semaine(1,"2021/05/10","2021/05/14");
$semaine2 = new semaine(2,"2021/05/17","2021/05/21");
$semaine3 = new semaine(3,"2021/05/24","2021/05/28");
$semaine4 = new semaine(4,"2021/05/31","2021/06/04");
$semaine5 = new semaine(5,"2021/06/07","2021/06/11");
$semaine6 = new semaine(6,"2021/06/14","2021/06/18");
$semaine7 = new semaine(7,"2021/06/21","2021/06/25";
$semaine8 = new semaine(8,"2021/06/28","2021/07/02");
$semaine9 = new semaine(9,"2021/07/05","2021/07/09");
$semaine10 = new semaine(10,"2021/07/12","2021/07/16");
$semaine11 = new semaine(11,"2021/07/19","2021/07/23");
$semaine12 = new semaine(12,"2021/07/26","2021/07/30");
$semaine13 = new semaine(13,"2021/08/02","2021/08/06");
$semaine14 = new semaine(14,"2021/08/09","2021/08/13");
$semaine15 = new semaine(15,"2021/08/19","2021/08/23");
?>