<?php
?>
<!DOCTYPE html>
<html>
    <?php include ("../misePage/header.php"); ?>
    <main>
    <div class="items">
        <h2>Le classique</h2>
        <p ><br>Le classique comprend chaque jour un bloc d’activités de type sportif et un autre avec une activité de
          type art et une activité de type science. </p>
        <h2><br>Prix: 1000.00 $ par semaine</h2>
      </div>

      <div class="items">
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>lundi</h2>
        <p ><br>La journée commance avec un bloc sportif comprenant combat, musculation et rudby. Le tout est suivi par un bloc mixe de science et
        d'art comprenant un cours en explosif et une scéance maquillage en cammouflage.  </p>
      </div>


    
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>mardi</h2>
        <p ><br>La journée commance avec un bloc sportif comprenant combat, musculation et rudby. Le tout est suivi par un bloc mixe de science et
        d'art comprenant un cours en conception de fusées et une scéance d'art culinaire avec le cours cuisine des végétaux.  </p>
      </div>


     
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>mercredi</h2>
        <p ><br>La journée commance avec un bloc sportif comprenant combat, musculation et rudby. Le tout est suivi par un bloc mixe de science et
        d'art comprenant un cours en corientation spatiale et une scéance d'art culinaire avec le cours cuisine des viandes.  </p>
      </div>
  

      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>jeudi</h2>
        <p ><br>La journée commance avec un bloc sportif comprenant combat, musculation et rudby. Le tout est suivi par un bloc mixe de science et
        d'art comprenant un cours en aviation et une scéance d'art de dessin anatomique.  </p>
      </div>
 
 
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>vendredi</h2>
        <p ><br>La journée commance avec un bloc sportif comprenant combat, musculation et rudby. Le tout est suivi par un bloc mixe de science et
        d'art comprenant un cours en programmation et une scéance d'art de dessin de cartes.  </p>
      </div>

      </main>
    <?php include ('../misePage/reseauSociaux.php'); ?>
    <?php include ('../misePage/banderole.php'); ?>
    <?php include ('../misePage/footer.php'); ?>
    <?php include ('../misePage/formulaire_connection.php'); ?>
    <?php include ('../misePage/formulaire_inscription.php'); ?>

    

</body>
</html>

