<?php
?>
<!DOCTYPE html>
<html>
    <?php include ('/../../misePage/header_admin.php'); ?>
<main>
<div class="tabs-wrapper">
    <!-- Onglet  -->
    <input type="radio" name="tab" id="tab4" class="tab-head" checked="checked"/>
    <label class="onglet" for="tab4">Comptes</label>
    <input type="radio" name="tab" id="tab3" class="tab-head" />
    <label class="onglet" for="tab3">Programmes</label>
    <input type="radio" name="tab" id="tab1" class="tab-head" />
    <label class="onglet" for="tab1">Inscriptions</label>
    <input type="radio" name="tab" id="tab2" class="tab-head" />
    <label class="onglet" for="tab2">Activité</label>


    <!--Ajouter bloc d'activités--> 
    <div class="tab-body-wrapper">
        <div id="tab-body-1" class="tab-body">
          <button type="button" class="collapsible">Annee</button>
          <div class="content-collapsible">
            <button type="button" class="collapsible">2020</button>
            <div class="content-collapsible">
              <button type="button" class="collapsible">Saison</button>
              <div class="content-collapsible">
                <button type="button" class="collapsible">ete</button>
                <div class="content-collapsible">

                </div>
                <button type="button" class="collapsible">hiver</button>

                <div class="content-collapsible">
                </div>
              </div>
            </div>
            
            <button type="button" class="collapsible">2021</button>
            <div class="content-collapsible">
            </div>
           
        </div>
      </div>
      
        <!--voir la liste des activités et blocs-->
        <div id="tab-body-2" class="tab-body">
          <button type="button" class="collapsible">Activités</button>
      <div class="content-collapsible">
      <p>Escalade<br>
        Natation <br>
        Cartographie<br></p>
      </div>
      <button type="button" class="collapsible">Bloc d'activités</button>
      <div class="content-collapsible">
        <button type="button" class="collapsible">sportif01</button>
        <div class="content-collapsible">
          <p>Escalade<br>
            Natation <br>
            Cartographie<br></p>
          </div>
          <button type="button" class="collapsible">artsci02</button>
          <div class="content-collapsible">
            <p>Escalade<br>
              Natation <br>
              Cartographie<br></p>
            </div>

        </div>
      </div>


        <!-- Voir les programme-->
        <div id="tab-body-3" class="tab-body">
           <button type="button" class="collapsible">L'actif</button>
    <div class="content-collapsible">
      <p>Description<br>
        Prix <br>
        Horaire<br></p>
    </div>
    <button type="button" class="collapsible">Le classique</button>
    <div class="content-collapsible">
      <p>Description<br>
        Prix <br>
        Horaire<br></p>
    </div>
    <button type="button" class="collapsible">Les arts et science</button>
    <div class="content-collapsible">
      <p>Description<br>
        Prix <br>
        Horaire<br></p>
    </div>
    </div>

               <!-- Voir Usagers et enfants-->
        <div id="tab-body-4" class="tab-body">

            <button type="button" class="collapsible">Personne 1</button>
            <div class="content-collapsible">
              <p>Prenom<br>
                Nom <br>
                Telephone<br></p>
                <button type="button" class="collapsible">Enfant 1</button>
            <div class="content-collapsible">
              <p>Prenom<br>
                Nom <br>
                Date fete<br></p>
            </div>
            </div>
            <button type="button" class="collapsible">Personne 2</button>
            <div class="content-collapsible">
              <p>Prenom<br>
                Nom <br>
                Telephone<br></p>
            </div>
        </div>
    </div>
</div>
</main>
<?php include ('/../../misePage/footer.php'); ?>

<!-- STAR EXTRA -->
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="openNav()"><span class="iconify" data-icon="ant-design:close-square-filled" data-inline="false" data-width="1.3vw" data-height="1.3vw"></span></a>
  <a href="javascript:void(0)" class="savebtn"><span class="iconify" data-icon="ant-design:save-filled" data-inline="false" data-width="1.3vw" data-height="1.3vw"></span></a>
  <a href="javascript:void(0)" class="resetbtn"><span class="iconify" data-icon="ri-delete-back-2-fill" data-inline="false" data-width="1.3vw" data-height="1.3vw"></span></a>
  <a  class="manager" href="#" onclick="document.getElementById('id01').style.display='block'">Ajouter une</br>session</a>
  <a  class="manager" href="#" onclick="document.getElementById('id02').style.display='block'">Ajouter un</br>programme </a>
  <a  class="manager" href="#" onclick="document.getElementById('id03').style.display='block'">Ajouter un</br>bloc</a>
  <a  class="manager" href="#" onclick="document.getElementById('id04').style.display='block'">Ajouter une</br>activité</a>
</div>

<!-- END EXTRA -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="scripts/main.js"></script>
<script src="scripts/slideshow.js"></script>
<script src="scripts/nav-dropdown.js"></script>
<script src="scripts/dropdown-checkbox.js"></script>
<script src="scripts/inscription.js"></script>
<script src="scripts/inscrireEnfant.js"></script>
<script src="scripts/collapsible.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="scripts/main.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
</body>
</html>