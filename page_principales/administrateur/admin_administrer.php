<?php
?>
<!DOCTYPE html>
<html>
    <?php include ('/../../misePage/header_admin.php'); ?>
<main>

  <div class="tabs-wrapper">
    <!-- Onglet (session-programme-bloc d'activite-activite) -->
    <input type="radio" name="tab" id="tab4" class="tab-head" checked="checked"/>
    <label class="onglet" for="tab4">Session</label>
    <input type="radio" name="tab" id="tab3" class="tab-head" />
    <label class="onglet" for="tab3">Programme</label>
    <input type="radio" name="tab" id="tab1" class="tab-head" />
    <label class="onglet" for="tab1">Bloc activités</label>
    <input type="radio" name="tab" id="tab2" class="tab-head" />
    <label class="onglet" for="tab2">Activité</label>


    <!--Ajouter bloc d'activités--> 
    <div class="tab-body-wrapper">
        <div id="tab-body-1" class="tab-body">
        <form class="modal-content animate" action="ajout_bloc.php" method="post">

          <button type="button" class="collapsible">Ajouter un bloc d'activité</button>
          <div class="content-collapsible">
              <div id="myDIV" class="headerlist">
                  <span onclick="" class="addBtn">Enregistrer</span>
              </div>
            
          <ul id="blcActivite">
            <li class="listitem">
                <label for="nomBloc"><b>Nom du bloc </b></label>
                <input type="text" id="nomBloc" name="nomBloc" placeholder="sportNautique04">
                <li class="listitem">
                    <label for="type"><b>Type</b></label>
                    <input type="text" id="type" name="type" placeholder="sportif">

                   <label for="nomActi"><b>Nom de l'activité</b></label>
                    <input type="text" id="nomActi" name="nomActi" placeholder="Escalade">
                </li>
                <li class="listitem">
                  <label for="type"><b>Type</b></label>
                  <input type="text" id="type" name="type" placeholder="sportif">

                 <label for="nomActi"><b>Nom de l'activité</b></label>
                  <input type="text" id="nomActi" name="nomActi" placeholder="Ballon chasseur">
                </li>
                <li class="listitem">
                  <label for="type"><b>Type</b></label>
                  <input type="text" id="type" name="type" placeholder="sportif">

                  <label for="nomActi"><b>Nom de l'activité</b></label>
                  <input type="text" id="nomActi" name="nomActi" placeholder="soccer">
               </li>
                <li class="listitem">
                  <label for="type"><b>Type</b></label>
                  <input type="text" id="type" name="type" placeholder="sportif">

                  <label for="nomActi"><b>Nom de l'activité</b></label>
                  <input type="text" id="nomActi" name="nomActi" placeholder="soccer">
                </li>
                <li class="listitem">
                  <label for="type"><b>Type</b></label>
                  <input type="text" id="type" name="type" placeholder="sportif">

                   <label for="nomActi"><b>Nom de l'activité</b></label>
                  <input type="text" id="nomActi" name="nomActi" placeholder="football">
                </li>
                <li class="listitem">
                  <label for="type"><b>Type</b></label>
                  <input type="text" id="type" name="type" placeholder="natation">

                  <label for="nomActi"><b>Nom de l'activité</b></label>
                  <input type="text" id="nomActi" name="nomActi" placeholder="polo">
                </li>
            </li>
            
        </ul>
        </div>
        </form>
      </div>
      
        <!-- Ajouter une activité-->
        <div id="tab-body-2" class="tab-body">
        <form class="modal-content animate" action="ajout_activite.php" method="post">

          <button type="button" class="collapsible">Ajouter une activité</button>
          <div class="content-collapsible">
              <div id="myDIV" class="headerlist">
                  <span onclick="" class="addBtn">Enregistrer</span>
              </div>
              <ul id="ajouterProgramme">
                  <li class="listitem">
                      <label for="nom"><b>Nom</b></label>
                      <input type="text" id="nom" name="nom" placeholder="escalade">

                      <label for="blocActivite"><b>Bloc d'activité associé</b></label>
                      <input type="text" id="blocActivite" name="blocActivite" placeholder="sports nautiques">

                      <label for="type"><b>Type</b></label>
                      <input type="text" id="type"  name="type" placeholder="sportif" >
                  </li>
              </ul>
            </div>
            </form>
        </div>

        <!-- Ajouter un programme-->
        <div id="tab-body-3" class="tab-body">
        <form class="modal-content animate" action="ajout_programme.php" method="post">

          <button type="button" class="collapsible">Ajouter un programme</button>
          <div class="content-collapsible">
              <div id="myDIV" class="headerlist">
                  <span onclick="" class="addBtn">Enregistrer</span>
              </div>

              <ul id="ajouterProgramme">
                <ul id="blcActivite">
                  <li class="listitem">
                      <label for="nomProg"><b>Nom programme </b></label>
                      <input type="text" id="nomProg" name="nomProg" placeholder="Le machiavel">

                      <label for="semaine"><b>Semaine </b></label>
                      <input type="number" id="semaine" name="semaine" min=1 max=52>

                      <li class="listitem">
                          <label for="bloc1"><b>Bloc activité</b></label>
                          <input type="text" id="bloc1" name="bloc1" placeholder="sportsNautique02">
                      </li>
                      <li class="listitem">
                        <label for="bloc2"><b>Bloc activité</b></label>
                        <input type="text" id="bloc2" name="bloc2" placeholder="sportsNautique02">
      

                      </li>
                      <li class="listitem">
                        <label for="bloc3"><b>Bloc activité</b></label>
                        <input type="text" id="bloc3" name="bloc3" placeholder="sportsNautique02">

                     </li>
                    
                  </li>

              </ul>
                
            </div>
            </form>
        </div>
               <!-- Ajouter une session-->
        <div id="tab-body-4" class="tab-body">
        <form class="modal-content animate" action="ajout_session.php" method="post">

            <button type="button" class="collapsible">Ajouter une session</button>
            <div class="content-collapsible">
                <div id="myDIV" class="headerlist">
                    <span onclick="" class="addBtn">Enregistrer</span>
                </div>

                <ul id="ajouterSession">
                    <li class="listitem">
                        <label for="annee"><b>Année</b></label>
                        <input type="text" id="annee" name="annee" placeholder="2020">

                        <label for="saison"><b>Saison </b></label>
                        <input type="text" id="saison" name="saison" placeholder="automne">

                        <label for="nbrSem"><b>Nombre de semaine</b></label>
                        <input type="number" id="nbrSem"  name="nbrSem" placeholder="01" min="1" max="52">

                    </li>

                </ul>
            </div>
            </form>
        </div>
    </div>
</div>
</main>
<?php include ('/../../misePage/footer.php'); ?>

<!-- STAR EXTRA -->
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="openNav()"><span class="iconify" data-icon="ant-design:close-square-filled" data-inline="false" data-width="1.3vw" data-height="1.3vw"></span></a>
  <a href="javascript:void(0)" class="savebtn"><span class="iconify" data-icon="ant-design:save-filled" data-inline="false" data-width="1.3vw" data-height="1.3vw"></span></a>
  <a href="javascript:void(0)" class="resetbtn"><span class="iconify" data-icon="ri-delete-back-2-fill" data-inline="false" data-width="1.3vw" data-height="1.3vw"></span></a>
  <a  class="manager" href="#" onclick="document.getElementById('id01').style.display='block'">Ajouter une</br>session</a>
  <a  class="manager" href="#" onclick="document.getElementById('id02').style.display='block'">Ajouter un</br>programme </a>
  <a  class="manager" href="#" onclick="document.getElementById('id03').style.display='block'">Ajouter un</br>bloc</a>
  <a  class="manager" href="#" onclick="document.getElementById('id04').style.display='block'">Ajouter une</br>activité</a>
</div>

<!-- END EXTRA -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="scripts/main.js"></script>
<script src="scripts/slideshow.js"></script>
<script src="scripts/nav-dropdown.js"></script>
<script src="scripts/dropdown-checkbox.js"></script>
<script src="scripts/inscription.js"></script>
<script src="scripts/inscrireEnfant.js"></script>
<script src="scripts/collapsible.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="scripts/main.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
<script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
</body>
</html>