<?php
?>
<!DOCTYPE html>
<html>
    <?php include ('../misePage/header_compte.php'); ?>
    
<main>
    <!-- Interface utilisateur -->
    <div class="tabs-wrapper">
        <!-- Onglet (Compte - Calendrier - Inscription - Facture) -->
        <input type="radio" name="tab" id="tab4" class="tab-head" checked="checked"/>
        <label class="onglet" for="tab4">Compte</label>
        <input type="radio" name="tab" id="tab3" class="tab-head" />
        <label class="onglet" for="tab3">Calendrier</label>
        <input type="radio" name="tab" id="tab1" class="tab-head" />
        <label class="onglet" for="tab1">Inscription</label>
        <input type="radio" name="tab" id="tab2" class="tab-head" />
        <label class="onglet" for="tab2">Facture</label>

        <!-- Inscription (Panier des programmes) -->
        <div class="tab-body-wrapper">
            <div id="tab-body-1" class="tab-body">
            <form class="modal-content animate" action="../post/enregistrement_inscription.php" method="post">

                <button type="submit" class="collapsible">Ajouter une semaine au panier</button>
                <div class="content-collapsible">
                    <div id="myDIV" class="headerlist">
                        <span onclick="" class="addBtn">Ajouter</span>
                    </div>

                    <ul id="inscrireEnfant">
                        <li class="listitem">
                            <label for="programme"><b>Programme  </b></label>
                            <input list="programme" name="programme">
                            <datalist id="programme">
                              <option value="L'actif">
                              <option value="Le classique">
                              <option value="Les arts et science">
                            </datalist><br>

                            <label for="semaine"><b>Semaine(voir section calendrier pour dates)  </b></label>
                            <input list="semaine" name="semaine">
                            <datalist id="semaine">
                              <option value="01">
                              <option value="02">
                              <option value="03">
                              <option value="04">
                              <option value="05">
                              <option value="06">
                              <option value="07">
                              <option value="08">
                              <option value="09">
                              <option value="10">
                              <option value="11">
                              <option value="12">
                              <option value="13">
                              <option value="14">
                              <option value="15">                                 
                            </datalist><br>
                           
                            <label for="enfant"><b>Enfant </b></label>
                            <input list="enfant" name="enfant">
                            <datalist id="enfant">
                                <!--ajouter methode qui display chacun des prenoms des enfants dans le compte et enlever le x-->
                              <option value="prenom">

                            </datalist><br>

                     
                        </li>

                    </ul>
                </div>
                </form>
            </div>

            <!-- Facture (Formulaire de payement) -->
            <div id="tab-body-2" class="tab-body">
                <div id="facture" class="row">
                    <div class="col-75">
                        <div class="price-container">
                            <form action="/action_page.php">

                                <div class="row">
                                    <div class="col-50">
                                        <h3>Adresse de facturations</h3>
                                        <label for="nomPrenom"><i class="fa fa-user"></i> Nom et prénom</label>
                                        <input type="text" id="nomPrenom" name="firstname" placeholder="Nom, prénom">
                                        <label for="email"><i class="fa fa-envelope"></i> E-mail</label>
                                        <input type="text" id="email" name="email" placeholder="exemple@email.com">
                                        <label for="adr"><i class="fa fa-address-card-o"></i> Adresse</label>
                                        <input type="text" id="adr" name="address" placeholder="1234 rue adresse">
                                        <label for="city"><i class="fa fa-institution"></i> Ville</label>
                                        <input type="text" id="city" name="city" placeholder="Ville">

                                        <div class="row">
                                            <div class="col-50">
                                                <label for="state">Province</label>
                                                <input type="text" id="state" name="state" placeholder="NY">
                                            </div>
                                            <div class="col-50">
                                                <label for="zip">Code postal</label>
                                                <input type="text" id="zip" name="zip" placeholder="A4B 3B7">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-50">
                                        <h3>Paiement</h3>
                                        <label>Cartes acceptées</label>
                                        <div class="icon-container">
                                            <i class="fa fa-cc-visa" style="color:navy;"></i>
                                            <i class="fa fa-cc-amex" style="color:blue;"></i>
                                            <i class="fa fa-cc-mastercard" style="color:red;"></i>
                                            <i class="fa fa-cc-discover" style="color:orange;"></i>
                                        </div>
                                        <label for="cname">Nom sur la carte</label>
                                        <input type="text" id="cname" name="cardname" placeholder="John More Doe">
                                        <label for="ccnum">Numéro de Carte de Crédit</label>
                                        <input type="text" id="ccnum" name="cardnumber" placeholder="1111-2222-3333-4444">
                                        <label for="expmonth">Mois d'expiration</label>
                                        <input type="text" id="expmonth" name="expmonth" placeholder="Septembre">
                                        <div class="row">
                                            <div class="col-50">
                                                <label for="expyear">Année d'expiration</label>
                                                <input type="text" id="expyear" name="expyear" placeholder="2023">
                                            </div>
                                            <div class="col-50">
                                                <label for="cvv">CVV</label>
                                                <input type="text" id="cvv" name="cvv" placeholder="352">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <label>
                                    <input type="checkbox" checked="checked" name="sameadr"> Adresse de livraison identique à la facturation
                                </label>
                                <input type="submit" value="Valider le paiement" class="btn">
<!--Dans le cadre du projet, nous n'iront pas plus loin pour le paiement-->
                            </form>
                        </div>
                    </div>
                    <div class="col-25">
                        <div class="price-container">
                            <h4>Programmes <span class="price" style="color:black"><i class="fa fa-shopping-cart"></i> <b>4</b></span></h4>
                            <p><a href="#">Le classique sem-01</a> <span class="price">$999.99</span></p>
                            <p><a href="#">Arts & science sem-01</a> <span class="price">$999.99</span></p>
                            <p><a href="#">L’enfant actif sem-01</a> <span class="price">$999.99</span></p>
                            <p><a href="#">Le classique sem-02</a> <span class="price">$999.99</span></p>
                            <hr>
                            <p>Total <span class="price" style="color:black"><b>$ - - - . - -</b></span></p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Calendrier (Agenda) -->
            <div id="tab-body-3" class="tab-body">
                <div class="agenda">
                    <div class="month">
                        <ul>
                            <li>
                                Horaire<br>
                                <span style="font-size:18px">Session été 2021</span>
                            </li>
                        </ul>
                    </div>

                    <ul class="">
                        <li>Semaine 01 : du 2021/05/10 au 2021/05/14</li>
                        <li>Semaine 02 : du 2021/05/17 au 2021/05/21</li>
                        <li>Semaine 03 : du 2021/05/24 au 2021/05/28</li>
                        <li>Semaine 04 : du 2021/05/31 au 2021/06/04</li>
                        <li>Semaine 05 : du 2021/06/07 au 2021/06/11</li>
                        <li>Semaine 06 : du 2021/06/14 au 2021/06/18</li>
                        <li>Semaine 07 : du 2021/06/21 au 2021/06/25</li>
                        <li>Semaine 08 : du 2021/06/28 au 2021/07/02</li>
                        <li>Semaine 09 : du 2021/07/05 au 2021/07/09</li>
                        <li>Semaine 10 : du 2021/07/12 au 2021/07/16</li>
                        <li>Semaine 11 : du 2021/07/19 au 2021/07/23</li>
                        <li>Semaine 12 : du 2021/07/26 au 2021/07/30</li>
                        <li>Semaine 13 : du 2021/08/02 au 2021/08/06</li>
                        <li>Semaine 14 : du 2021/08/09 au 2021/08/13</li>
                        <li>Semaine 15 : du 2021/08/19 au 2021/08/23</li>
                    </ul>

                    
                </div>
                </div>
            <div id="tab-body-4" class="tab-body">
            <form class="modal-content animate" action="../post/enregistrement_enfant.php" method="post">
                <button type="submit" class="collapsible">Ajouter un enfant</button>
                <div class="content-collapsible">
                    <div id="myDIV" class="headerlist">
                        <span onclick="" class="addBtn">Enregistrer</span>
                    </div>

                    <ul id="inscrireEnfant">
                        <li class="listitem">
                            <label for="firstname"><b>Prénom</b></label>
                            <input type="text" id="firstname" name="firstname" placeholder="Votre prénom...">

                            <label for="fistname"><b>Nom </b></label>
                            <input type="text" id="fistname" name="firstname" placeholder="Votre nom...">

                            <label for="yearpicker1"><b>Date de naissance</b></label>
                            <input type="date" id="yearpicker1" pattern="\d{4}-\d{0}-\d{0}" min="2000-04-01" max="2017-04-30">

                        </li>

                    </ul>
                </div>
            </div>
            </form>

        </div>
    </div>
      </main>
    <?php include ('../misePage/reseauSociaux.php'); ?>
    <?php include ('../misePage/banderole.php'); ?>
    <?php include ('../misePage/footer.php'); ?>

    

</body>
</html>