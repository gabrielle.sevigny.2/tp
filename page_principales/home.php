<!--Liste des programmes-->
        <div class="items">
      <!--Exemple de programme complet-->
      <div class="card complet ">

        <img src="images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>classique</h2>
        <p class="description"><br>Le classique comprend chaque jour un bloc d’activités de type sportif et un autre avec une activité de
          type art et une activité de type science. </p>
        <div style="margin: 24px 0;">
          <span class="iconify" data-icon="ic:baseline-sports" data-inline="false"></span>
          <span class="iconify" data-icon="emojione-monotone:artist-palette" data-inline="false"></span>
          <span class="iconify" data-icon="ic:outline-science" data-inline="false"></span>
        </div>
        <a href="page_principales/classique.php"><button >Voir</button></a>
      </div>
      <div class="card complet ">
 
        <img src="images/square.png" alt="image"  width="0"  height="0">
        <h2>Les arts et<br>la science</h2>

        <p class="description"><br>Le programme arts et science comprend plusieurs activités d'arts culinaires, d'arts visuels, d'arts plastiques,
          de chimie, de biologie et de physique. Il ne possède pas d’activité physique, cependant une activité
          matinale est réservée pour pratiquer le yoga ou jouer à un jeu de course comme le ballon chasseur. </p>
        <div style="margin: 24px 0;">
          <span class="iconify" data-icon="mdi:yoga" data-inline="false"></span>
          <span class="iconify" data-icon="mdi:chef-hat" data-inline="false"></span>
          <span class="iconify" data-icon="maki:cinema-15" data-inline="false"></span>
          <span class="iconify" data-icon="emojione-monotone:artist-palette" data-inline="false"></span>
          <span class="iconify" data-icon="fa-solid:volleyball-ball" data-inline="false"></span>
          <span class="iconify" data-icon="ic:baseline-biotech" data-inline="false"></span>
          <span class="iconify" data-icon="ic:outline-science" data-inline="false"></span>
        </div>
        <a href="page_principales/art_science.php"><button >Voir</button></a>
      </div>
      <div class="card complet " >

        <img src="images/square.png" alt="image" width="0"  height="0">
        <h2>L’enfant<br>actif</h2>

        <p class="description"><br>Le programme athlétique est un camp de jour sportif intensif pour les enfants très actifs. Il comprend au
          moins quatre activités quotidiennes dont le basketball, le tennis, le soccer, le ballon chasseur, le baseball,
          etc.</p>
        <div style="margin: 24px 0;">
          <span class="iconify" data-icon="fluent:sport-soccer-20-filled" data-inline="false"></span>
          <span class="iconify" data-icon="bx:bxs-basketball" data-inline="false"></span>
          <span class="iconify" data-icon="emojione-monotone:tennis" data-inline="false"></span>
          <span class="iconify" data-icon="fa-solid:volleyball-ball" data-inline="false"></span>
          <span class="iconify" data-icon="cil:baseball" data-inline="false"></span>
        </div>
        <a href="page_principales/actif.php"><button >Voir</button></a>
      </div>