<?php
?>
<!DOCTYPE html>
<html>
    <?php include ('../misePage/header.php'); ?>
    <main>
    <div class="items">
        <h2>L'actif</h2>
        <p ><br>Le programme athlétique est un camp de jour sportif intensif pour les enfants très actifs. Il comprend au
          moins quatre activités quotidiennes tel le basketball, le tennis, le soccer, le ballon chasseur, le baseball,
          etc. </p>
        <h2><br>Prix: 1000.00 $ par semaine</h2>
      </div>

      <div class="items">
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>lundi</h2>
        <p ><br>La journée commance avec un entrainement en musculation avant de faire une scéance de combat. Le tout est suivi d'une partie de 
        rudby et la journée se termine avec une scéance de tire au fusil. </p>
      </div>


    
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>mardi</h2>
        <p ><br>La journée commance avec un entrainement en musculation avant de faire une scéance de combat. Le tout est suivi d'une partie de 
        rudby et la journée se termine avec une de course à obstacle.</p>
      </div>


     
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>mercredi</h2>
        <p ><br>La journée commance avec un entrainement en musculation avant de faire une scéance de combat. Le tout est suivi d'une seconde scéance
        d'entraînement en salle avant de faire une sortie en parachute et finir la journée en jouant au rudby avec les amis. </p>
      </div>
  

      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>jeudi</h2>
        <p ><br>La journée commance avec un entrainement en musculation avant de faire une scéance de combat. Le tout est suivi d'une partie de 
        rudby et la journée se termine avec une scéance de tire au fusil.   </p>
      </div>
 
 
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>vendredi</h2>
        <p ><br>La journée commance avec un entrainement en musculation avant de faire une scéance de combat. Le tout est suivi d'une séance de 
        parachutiste et la journée se termine avec une course à obstacle.  </p>
      </div>
   
    
      </main>
    <?php include ('../misePage/reseauSociaux.php'); ?>
    <?php include ('../misePage/banderole.php'); ?>
    <?php include ('../misePage/footer.php'); ?>
    <?php include ('../misePage/formulaire_connection.php'); ?>
    <?php include ('../misePage/formulaire_inscription.php'); ?>

    

</body>
</html>
