<?php
?>
<!DOCTYPE html>
<html>
    <?php include ('../misePage/header.php'); ?>
    <main>
    <div class="items">
        <h2>À propos de nous</h2>
        <p ><br>Le camp de jour opère à présent depuis plus de 15 ans. Nous avons une équipe dévouée et attentionnée de sergeants qui 
       prennent soins de vos petits. Leur bien-être et leur formation est notre principale prioritée. Lors de cette semaine extraordinaire,
        ils vivront des activités uniques qu'aucun autre camp de jour offre. Ils en ressortiront plus forts et disciplinés.  <br>
        <h2>Contact</h2><br>
      Téléphone: 514-555-5555<br>
      Email: campdejourmilitaire@gmail.com <br>
      Adresse: 666 rue belvédère Montréal Québec Canada H19 W8I 
     </p>

      </div>
      </main>
    <?php include ('../misePage/reseauSociaux.php'); ?>
    <?php include ('../misePage/banderole.php'); ?>
    <?php include ('../misePage/footer.php'); ?>
    <?php include ('../misePage/formulaire_connection.php'); ?>
    <?php include ('../misePage/formulaire_inscription.php'); ?>

</body>
</html>