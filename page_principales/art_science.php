<?php
?>
<!DOCTYPE html>
<html>
    <?php include ('../misePage/header.php'); ?>
    <main>
    <div class="items">
        <h2>Les arts et la science</h2>
        <p ><br>Le programme arts et science comprend plusieurs activités d'arts culinaires, d'arts visuels, d'arts plastiques,
          de chimie, de biologie et de physique. Il ne possède pas d’activité physique, cependant une activité
          matinale est réservée pour pratiquer le yoga ou jouer à un jeu de course comme le ballon chasseur. . </p>
        <h2><br>Prix: 1000.00 $ par semaine</h2>
      </div>

      <div class="items">
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>lundi</h2>
        <p ><br>La journée commance avec une scéance matinale de yoga. S'en suit un moment maquillage dans le cours de cammouflage. Ils
        pourront par la suite explorer l'art culinaire de la cuisine des végétaux puis se faire aller le crayon dans le cours de dessin
        anatomique. La journée termine avec un gros bang dans le cours scientifique d'explosif.
        </p>
      </div>


    
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>mardi</h2>
        <p ><br>La journée commance avec une scéance matinale de jogging. S'en suit un moment science dans le cours de conception de drones. Ils
        pourront par la suite explorer l'art de la programmation  . La journée termine avec un cours de cuisine de champignons sauvages.  </p>
      </div>


     
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>mercredi</h2>
        <p ><br>La journée commance avec une scéance matinale de yoga. S'en suit un moment science dans le cours d'orientation spatiale. Ils
        pourront par la suite explorer l'art du dessin cartographique. La journée termine avec un cours de cuisine de la viande sauvages. </p>
      </div>
  

      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>jeudi</h2>
        <p ><br>La journée commance avec une scéance matinale de jogging. S'en suit un cours d'explosif. Ils
        pourront par la suite explorer les airs dans le cours d'aviation  puis se faire aller le cerveau dans le cours de conception de drone.
         La journée termine avec un cours de cuisine des végétaux sauvages.  </p>
      </div>
 
 
      <div class="card complet ">
        <img src="/tp2/test/images/square.png" alt="image" width="0"  height="0">
        <h2>Le<br>vendredi</h2>
        <p ><br>La journée commance avec une scéance matinale de yoga. S'en suit un cours d'aviation. Ils
        pourront par la suite explorer l'art du cammouflagen  puis se faire aller le crayon dans le cours de dessin anatomique.
          </p>
      </div>
   
    
      </main>
    <?php include ('../misePage/reseauSociaux.php'); ?>
    <?php include ('../misePage/banderole.php'); ?>
    <?php include ('../misePage/footer.php'); ?>
    <?php include ('../misePage/formulaire_connection.php'); ?>
    <?php include ('../misePage/formulaire_inscription.php'); ?>

    

</body>
</html>

