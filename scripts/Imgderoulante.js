var images = ["images/Picture2.png", "images/Picture3.png", "images/Picture4.png", "images/Picture5.png", "images/Picture1.png"];
var $img = $("#img"), i = 0, speed = 200;
window.setInterval(function() {
    $img.fadeOut(speed, function() {
        $img.attr("src", images[(++i % images.length)]);
        $img.fadeIn(speed);
    });
}, 5000);