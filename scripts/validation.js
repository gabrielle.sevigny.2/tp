function validationString(mot){
    var lettres = /^[a-zA-Z]+ [a-zA-Z]+$/;
    if (empty(mot)) {
        alert( "champs vide");
        return false;
    }else if(mot.value.match(lettres)){
        return true;
    }else {
        alert("Doit contenir des lettres seulement");
        mot.focus();
        return false;
    }
}

function validationMail(courriel){
var courrielformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
if (empty(courriel)) {
    alert( "champs vide");
    return false;
}else if(courriel.value.match(courrielformat)){
    return true;
}else{
    alert("L'adresse courriel est de format invalide");
    courriel.focus();
    return false;
    }
}

function validationAdresse(adresse){ 
var charac = /^[0-9a-zA-Z]+$/;
if (empty(adresse)) {
    alert( "champs vide");
    return false;
}else if(adresse.value.match(charac))
{return true;
}else{
    alert('Characters invalides');
    adresse.focus();
    return false;
    }
}

function validationDate(date){

var valeurs = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(0[1-9]|1[1-9]|2[1-9])$/;
if (empty(date)) {
    alert( "champs vide");
    return false;
}else if(date.value.match(valers)) {
        return true;        }
else{
    alert('Format de date invalide');
    date.focus();
    return false;
        }
}

function validationTelephone(telephone){
  var character = /^\d{10}$/;
  if (empty(telephone)) {
    alert( "champs vide");
    return false;
}else if(telephone.value.match(character)){
      return true;
    }else{
        alert("Format de telephone non valide");
        return false;
  }
}
