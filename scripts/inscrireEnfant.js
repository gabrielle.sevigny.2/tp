// Create a "close" button and append it to each list item
var myNodelist = document.getElementsByClassName("listitem");
var i;
for (i = 0; i < myNodelist.length; i++) {
    var span = document.createElement("span");
    var txt = document.createTextNode("\u00D7");
    span.className = "closelist";
    span.appendChild(txt);
    myNodelist[i].appendChild(span);
}

// Click on a close button to hide the current list item
var close = document.getElementsByClassName("closelist");
var i;
for (i = 0; i < close.length; i++) {
    close[i].onclick = function() {
        var div = this.parentElement;
        div.style.display = "none";
    }
}

// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('#inscrireEnfant');
list.addEventListener('click', function(ev) {
    if (ev.target.tagName === 'LI') {
        ev.target.classList.toggle('checked');
    }
}, false);

// Create a new list item when clicking on the "Add" button
function newEnfant() {
    var li = document.createElement("li");
    li.classList.add("listitem");
    var inputValue = document.getElementById("inscrireEnfant").value;

    li.innerHTML += "<label for=\"fname\"><b>Prénom</b></label>\n" +
        "              <input type=\"text\" id=\"fname\" name=\"firstname\" placeholder=\"Votre prénom...\">\n" +
        "\n" +
        "              <label for=\"fname\"><b>Nom </b></label>\n" +
        "              <input type=\"text\" id=\"fname\" name=\"firstname\" placeholder=\"Votre nom...\">\n" +
        "\n" +
        "              <label for=\"yearpicker\"><b>Date de naissance</b></label>\n" +
        "              <input type=\"date\" pattern=\"\\d{4}-\\d{0}-\\d{0}\" min=\"2000-04-01\" max=\"2017-04-30\">"


        document.getElementById("inscrireEnfant").appendChild(li);


    var span = document.createElement("span");
    var txt = document.createTextNode("\u00D7");
    span.className = "closelist";
    span.appendChild(txt);
    li.appendChild(span);

    for (i = 0; i < close.length; i++) {
        close[i].onclick = function() {
            var div = this.parentElement;
            div.style.display = "none";
        }
    }
}