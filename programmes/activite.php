<?php
require_once('blocActivite.php');
require_once('programme.php');
class activite extends blocActivite {
    function _contruct($description, $type){
        $this -> description = $description;
        $this -> type= $type;
    }

    function setDescription($description){
        $this->description=$description;
    }

    function getDescription() {
        return $this->description;
    }

    function setType($type){
        $this->type=$type;
    }

    function getType() {
        return $this->type;
    }
}

$parachute = new activite ("parachute", "sportif");
$tireFusil = new activite ("tireFusil", "sportif");
$courseObstacle = new activite ("course a obstacle", "sportif");
$musculaton = new activite ("musculation", "sportif");
$rudby = new activite ("rudby", "sportif");
$combat = new activite ("combat", "sportif");


$explosif = new activite ("explosif", "science");
$fusee = new activite ("conception de fusees", "science");
$orientationSpaciale = new activite ("orientation spaciale", "science");
$programmation = new activite ("programmation logiciel", "science");
$aviation = new activite ("aviation", "science");
$drone = new activite ("conception de drone", "science");

$cammouflage = new activite ("cammouflage", "art");
$cuisineVegetaux = new activite ("cuisine les vegetaux", "art");
$anatomie = new activite ("dessin anatomie", "art");
$cartographie = new activite ("dessin cartes", "art");
$mycologie = new activite ("cuisiner les champignons", "art");
$cuisineViande = new activite ("cuisiner la viande", "art");

$yoga = new activite ("yoga", "activite Matinale");
$jogging = new activite ("jogging", "activite Matinale");
?>