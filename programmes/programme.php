<?php

class programme {
    function _contruct($description, $prix, $horaire, $activite, $moniteur,$listeEnfant){
        $this -> description = $description;
        $this -> prix = $prix;
        $this ->horaire = $horaire;
        $this ->activite = $activite;
        $this ->moniteur = $moniteur;
        $this ->listeEnfant = $listeEnfant;
    }

    function setDescription($description){
        $this->description=$description;
    }

    function getDescription() {
        return $this->description;
    }

    function setPrix($prix){
        $this->prix=$prix;
    }

    function getPrix() {
        return $this->prix;
    }

    function setHoraire($horaire){
        $this->horaire=$horaire;
    }

    function getHoraire() {
        return $this->horaire;
    }

    function setActivite($activite){
        $this->activite=$activite;
    }

    function getActivite() {
        return $this->activite;
    }

    function setMoniteur($moniteur){
        $this->moniteur=$moniteur;
    }

    function getMoniteur() {
        return $this->moniteur;
    }

    function setListeEnfant($listeEnfant){
        $this->listeEnfant=$listeEnfant;
    }

    function getListeEnfant() {
        return $this->listeEnfant;
    }
}
$classique = new programme ("Le classique comprend chaque jour un bloc d’activités de type sportif et un autre avec une activité de
type art et une activité de type science.", 1000.00,"Lundi: combat - musculation - rudby - explosif - cammouflage, 
Mardi: combat - musculation - rudby - conception fusee - cuisine vegetaux, 
Mercredi:combat - musculation - rudby - orientation spatiale - cuisine viande , 
Jeudi:combat - musculation - rudby - aviation - dessin d'anatomie, Vendredi:combat - musculation - rudby - programmation - dessin carte", 
[$blocSportif3, $blocMixe2SA1,$blocMixe2SA2,$blocMixe2SA3,$blocMixe2SA4,$blocMixe2SA5]),
"Sergeant TopGear",[];

$artScience = new programme ("Le programme arts et science comprend plusieurs activités d'arts culinaires, d'arts visuels, d'arts plastiques,
de chimie, de biologie et de physique. Il ne possède pas d’activité physique, cependant une activité
matinale est réservée pour pratiquer le yoga ou jouer à un jeu de course comme le ballon chasseur. ", 1250.00,
"Lundi: yoga-cammouflage - cuisine des vegetaux - dessin d'anatomie - explosif, 
Mardi: jogging - conception drone - programmation - cuisiner les champignons, 
Mercredi:yoga - orientation spatiale - dessin carte - cuisiner la viande , 
Jeudi:jogging - explosif - aviation - conception drone - cuisine les vegetaux, 
Vendredi:yoga - aviation - cammouflage - dessins  d'anatomie", 
[$yoga, $jogging,$cammouflage,$explosif,$fusee,$orientationSpatiale,$programmation,$aviation,$drone,$cuisineVegetaux,
$anatomie,$cartographie, $mycologie, $cuisineViande]),
"Sergeant Topgun",[];

$enfantActif = new programme ("Le programme athlétique est un camp de jour sportif intensif pour les enfants très actifs. Il comprend au
moins quatre activités quotidiennes dont le basketball, le tennis, le soccer, le ballon chasseur, le baseball,
etc ",
 640.00,
"Lundi: musculation-combat-rudby-tire au fusil, Mardi: musculation-combat-rudby-course a obstacle, 
Mercredi:musculation-combat-muculation-parachute-rudby, 
Jeudi:musculation-combat-rudby-tire au fusil, Vendredi:musculation-combat-parachute-course a obstacle", 
[$parachute, $tireFusil,$courseObstacle,$musculaton,$combat,$rudby]),
"Sergeant CryBaby",[];

?>


