<!DOCTYPE html>
<html lang="fr">
  <head>
    <!--Page par default(aucun client ou admin identifier) -->
    <title>Accueil</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Style personnaliser -->
    <link rel="stylesheet" href="/tp2/test/styles/principal.css">
    <link rel="stylesheet" href="/tp2/test/styles/header.css">
    <link rel="stylesheet" href="/tp2/test/styles/main.css">
    <link rel="stylesheet" href="/tp2/test/styles/footer.css">
    <link rel="stylesheet" href="/tp2/test/styles/slideshow.css">
    <link rel="stylesheet" href="/tp2/test/styles/modal.css">
    <link rel="stylesheet" href="/tp2/test/styles/tooltip.css">
    <link rel="stylesheet" href="/tp2/test/styles/item.css">
    <link rel="stylesheet" href="/tp2/test/styles/calendar.css">
    <link rel="stylesheet" href="/tp2/test/styles/socialbar.css">
    <link rel="stylesheet" href="/tp2/test/styles/sign-up.css">
    <!-- Librairie importer -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Type d'écriture importer -->
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
  </head>
  <body>
  <header>
    <!-- entete avant plan -->
    <div class="header-front">
      <div id="logo-01">
        <div id="logo-part01"><h1>Camp de Jour</h1></div>
        <div id="logo-part02"><h2>ÉTÉ 2021</h2></div>
      </div>
      <div class="topnav">
        <div class="header-right">
          <a class="first tooltip" href="#home" onclick="document.getElementById('id01').style.display='block'">
          <i class="fa fa-user"></i><span class="tooltiptext">Compte</span></a>
        </div>
      </div>

      <div class="downnav menu-area">
        <ul>
          <li class="flex-1 text-white">
            <a href="/tp2/test/index.php">Accueil</a>
          </li>
          <li class="flex-1 text-white"><a href="#">Programmes&nbsp;<i class="fa fa-caret-down"></i></a>
            <ul class="dropdown dropdown-1">
              <li><a href="/tp2/test/page_principales/classique.php">Le classique</a></li>
              <li><a href="/tp2/test/page_principales/art_science.php">Les arts et la science</a></li>
              <li><a href="/tp2/test/page_principales/actif.php">L’enfant actif</a></li>
            </ul>
          </li>
          <li class="flex-1 text-white"><a href="/tp2/test/page_principales/a_propos.php">Info/Contact</a></li>
          <li class="flex-3 text-white inactiveLink"><a href="#"></a></li>
        </ul>
      </div>
    </div>
    <!-- entete arriere plan avec bande déroulante-->
    <div class="header-back slideshow-container">
      <div class="mySlides1">
        <p>Le camp de jour offre pour la session d’été 2021 trois différentes programmations par semaine.
          Un programme a une durée d’une semaine et la session possède 15 semaines. </p>
        <img class="opacity-image-75" src="/tp2/test/images/science-banner.jpg" alt="image"  width="0" height="0">
      </div>
      <div class="mySlides1">
        <p>Un enfant peut être inscrit
          au camp de jour pendant une ou plusieurs semaines. </p>
        <img class="opacity-image-75" src="/tp2/test/images/sport-banner.jpg" alt="image"  width="0" height="0">
      </div>
      <div class="mySlides1">
        <p>Un programme possède un horaire de 5 jours qui
          comprend quotidiennement des d’activités et/ou des blocs d’activités qui sont composés d’activités.
          Chaque programme possède au maximum 6 activités par
          jour.
        </p>
        <img class="opacity-image-75" src="/tp2/test/images/art-banner.jpg" alt="image"  width="0" height="0">
      </div>
      <a class="prev" onclick="plusSlides(-1, 0)">&#10094;</a>
      <a class="next" onclick="plusSlides(1, 0)">&#10095;</a>
    </div>
  </header>
  <body>
