<footer>
  <div class="bo-wrap clr4">
    <div class="bo-footer">
      <div class="bo-footer-social"></div>
    </div>
  </div>
  <div class="bo-wrap clr3">
    <div class="bo-footer">
      <div class="bo-footer-smap">
       <a href="/tp2/test/page_principales/a_propos.php">Contactez-nous</a>
      </div>

      <div class="bo-footer-power">
        Fait par <b><a href="#"> Omar Amari</a>, <a href="#">Gabrielle Sévigny</a>, <a href="#">Ibrahim Malick</a> & <br><a href="#">Koffi Vivien Fabrice Aïssetche</a></b>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="bo-wrap clr4">
    <div class="bo-footer">
      <div class="bo-footer-copyright">INF3190 - INTRODUCTION À LA PROGRAMMATION WEB<br>2021 <i class="fa fa-university"></i> Universitvé du Québec à Montréal</div>
    </div>
  </div>
</footer>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://code.iconify.design/1/1.0.7/iconify.min.js"></script>
  <script src="/tp2/test/scripts/main.js"></script>
  <script src="/tp2/test/scripts/slideshow.js"></script>
  <script src="/tp2/test/scripts/nav-dropdown.js"></script>
  <script src="/tp2/test/scripts/modal-login.js"></script>
  <script  src="/tp2/test/scripts/modal-inscription.js"></script>
  <script src="/tp2/test/scripts/Imgderoulante.js"></script>