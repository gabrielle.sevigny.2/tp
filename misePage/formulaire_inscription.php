<!-- J,ai essayer de faire une bande deroulante mais avec echec-->
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
/* width */
::-webkit-scrollbar {
  width: 20px;
}

/* Track */
::-webkit-scrollbar-track {
  box-shadow: inset 0 0 5px grey; 
  border-radius: 10px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: red; 
  border-radius: 10px;
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #b30000; 
}
</style>
</head>    
<body>

<div id="id02" class="modal">
    <form class="modal-content animate" action="../post/enregistrement_usager.php" method="post">
      <div class="imgcontainer">
        <span onclick="document.getElementById('id02').style.display='none'" class="close" title="Close Modal">&times;</span>
        <div class="empty-512"><h2>S'inscrire</h2><p><br>Veuillez remplir ce formulaire pour créer un compte.</p></div>
      </div>
      <div class="modal-container">
        <label for="prenom"><b>Prenom</b></label>
        <input type="text" id="prenom" placeholder="Prenom" name="prenom" required>

        <label for="nom"><b>Nom</b></label>
        <inp
        ut type="text" id="nom" placeholder="nom" name="nom" required>

        <label for="adresse"><b>Adresse</b></label>
        <input type="text" id="adresse" placeholder="123 rue ville province pays" name="adresse" required>

        <label for="email"><b>E-mail</b></label>
        <input type="email" id="email" placeholder="exemple@email.com" name="email" required>

        <label for="dNaissance"><b>Date de naissance</b></label>
        <input type="date" id="dNaissance" pattern="\d{4}-\d{0}-\d{0}" min="1900-04-01" max="2017-04-30"  name="dNaissance" required>

        <label for="psw"><b>Mot de passe</b></label>
        <input type="password" id="psw" placeholder="*******" name="psw" required>

        <label for="psw-repeat"><b>Répéter le mot de passe</b></label>
        <input type="password" id="psw-repeat" placeholder="*******" name="psw-repeat" required>

        <label>
          <input type="checkbox" name="remember" style="margin-top:25px" required> En créant un compte, vous acceptez nos <a href="#" style="color:dodgerblue">Conditions et confidentialité</a>.
        </label>
        <button type="submit">S'inscrire</button>
      </div>

      <div class="modal-container" style="background-color:#f1f1f1">
        <button onclick="document.getElementById('id02').style.display='none'" class="cancelbtn">Annulé</button>
      </div>
    </form>
  </div>
</body>