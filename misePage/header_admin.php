<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Page Title</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CUSTOM STYLES -->
  <link rel="stylesheet" href="styles/principal.css">
  <link rel="stylesheet" href="styles/header.css">
  <link rel="stylesheet" href="styles/main.css">
  <link rel="stylesheet" href="styles/footer.css">
  <link rel="stylesheet" href="styles/slideshow.css">
  <link rel="stylesheet" href="styles/modal.css">
  <link rel="stylesheet" href="styles/tooltip.css">
  <link rel="stylesheet" href="styles/item.css">
  <link rel="stylesheet" href="styles/socialbar.css">
  <link rel="stylesheet" href="styles/sidenav.css">
  <link rel="stylesheet" href="styles/sidenav-second.css">
  <link rel="stylesheet" href="styles/onglet.css">
  <link rel="stylesheet" href="styles/inscription.css">
  <link rel="stylesheet" href="styles/inscrireEnfant.css">
  <link rel="stylesheet" href="styles/collapsible.css">
  <link rel="stylesheet" href="styles/contact.css">
  <!-- IMPORTED STYLES -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- IMPORTED FONTS -->
  <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
</head>
<body>
<header>
  <div class="header-front">
    <div id="logo-01">
      <div id="logo-part01"><h1>Camp de Jour</h1></div>
      <div id="logo-part02"><h2>ÉTÉ 2021</h2></div>
    </div>
    <div class="topnav">
      <div class="header-right">
        <a class="inactiveLink active first tooltip" href="/../page_principales/administrateur/admin_administrer.php"><i class="fas fa-user-cog"></i><span class="tooltiptext2">Administrer</span></a>
        <a class="first tooltip" href="" onclick="openNav()"><i class="fas fa-edit"></i><span class="tooltiptext2">Editer</span></a>
        <a class="first logout tooltip" href="/../page_principales/administrateur/admin_editer.php" ><i class="fas fa-sign-out-alt"></i><span class="tooltiptext">Déconnexion</span></a>
      </div>
    </div>
    <div class="downnav menu-area">
      <ul>
        <li class="flex-1 text-white"><a href="#">Retour</a></li>
        <li class="flex-3 text-white inactiveLink"><a href="#"></a></li>
        <li class="flex-1 text-white inactiveLink"><a href="#"></a></li>
        <li class="flex-1 text-white inactiveLink"><a href="#"></a>
          <ul class="dropdown dropdown-1">
            <li><a href="#">service 1</a></li>
            <li><a href="#">service 2</a></li>
            <li><a href="#">service 3</a></li>
          </ul>
        </li>
        <li class="flex-1 text-white inactiveLink"><a href="#"></a></li>


      </ul>
    </div>
  </div>
  <div class="header-back slideshow-container">
    <div class="mySlides1">
      <p>Le camp de jour offre pour la session d’été 2021 trois différentes programmations par semaine.
        Un programme a une durée d’une semaine et la session possède 15 semaines. </p>
      <img class="opacity-image-75" src="images/science-banner.jpg" width="100%" height="auto">
    </div>
    <div class="mySlides1">
      <p>Un enfant peut être inscrit
        au camp de jour pendant une ou plusieurs semaines. </p>
      <img class="opacity-image-75" src="images/sport-banner.jpg" width="100%" height="auto">
    </div>
    <div class="mySlides1">
      <p>Un programme possède un horaire de 5 jours qui
        comprend quotidiennement des d’activités et/ou des blocs d’activités qui sont composés d’activités.
        Chaque programme possède au maximum 6 activités par
        jour.
      </p>
      <img class="opacity-image-75" src="images/art-banner.jpg" width="100%" height="auto">
    </div>
    <a class="prev" onclick="plusSlides(-1, 0)">&#10094;</a>
    <a class="next" onclick="plusSlides(1, 0)">&#10095;</a>
  </div>
</header>
<body>
