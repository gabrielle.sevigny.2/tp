<div id="id01" class="modal">
    <form class="modal-content animate" action="../post/connection.php" method="post">
      <div class="imgcontainer">
        <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
        <img src="/tp2/test/images/man_profile_boy_avatar_human_people_smile-512.png" alt="Avatar" class="avatar">
      </div>

      <div class="modal-container">
        <label for="email"><b>Adresse Courriel</b></label>
        <input type="text" id="email" placeholder="Entrer votre courriel" name="email" required>

        <label for="psw"><b>Mot de passe</b></label>
        <input type="password" id="psw" placeholder="Entrer votre mot de passe" name="psw" required>

        <button type="submit">Connexion</button>
        <label>
          <input type="checkbox" checked="checked" name="remember"> Souviens-toi
        </label>
      </div>

      <div class="modal-container cree-compte">
        <label for="uname"><b>OU</b></label>
        <button onclick="document.getElementById('id02').style.display='block'">Crée un compte</button>
      </div>

      <div class="modal-container" style="background-color:#f1f1f1">
        <button onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">Annulé</button>
        <span class="psw"><a id="surligner" href="#"><u>Mot de passe</u></a> oublié?</span>
      </div>
    </form>
  </div>